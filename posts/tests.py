from django.test import TestCase
from .models import Post
# Create your tests here.
class PostModelTest(TestCase):
    def setUp(self):
        post.objects.create(text="just a test")

    def test_text_context(self):
        post = post.object.get(id=1)
        expected_text = post.text 
        self.assertDictEqual(expected_text, "just a test")